﻿namespace Image_Grid_Viewer
{
    partial class ToolBoxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox_Grid = new System.Windows.Forms.GroupBox();
            this.PictureBox_Color = new System.Windows.Forms.PictureBox();
            this.Button_Row_Dec = new System.Windows.Forms.Button();
            this.Button_Column_Dec = new System.Windows.Forms.Button();
            this.Button_Row_Inc = new System.Windows.Forms.Button();
            this.Button_Column_Inc = new System.Windows.Forms.Button();
            this.TextBox_Row = new System.Windows.Forms.TextBox();
            this.Label_Color = new System.Windows.Forms.Label();
            this.Label_Row = new System.Windows.Forms.Label();
            this.TextBox_Column = new System.Windows.Forms.TextBox();
            this.Label_Column = new System.Windows.Forms.Label();
            this.ColorDialogue = new System.Windows.Forms.ColorDialog();
            this.GroupBox_Scale = new System.Windows.Forms.GroupBox();
            this.Button_Fit = new System.Windows.Forms.Button();
            this.Button_Origin = new System.Windows.Forms.Button();
            this.Button_ZoomOut = new System.Windows.Forms.Button();
            this.Button_ZoomIn = new System.Windows.Forms.Button();
            this.TextBox_Scale = new System.Windows.Forms.TextBox();
            this.GroupBox_Grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Color)).BeginInit();
            this.GroupBox_Scale.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox_Grid
            // 
            this.GroupBox_Grid.Controls.Add(this.PictureBox_Color);
            this.GroupBox_Grid.Controls.Add(this.Button_Row_Dec);
            this.GroupBox_Grid.Controls.Add(this.Button_Column_Dec);
            this.GroupBox_Grid.Controls.Add(this.Button_Row_Inc);
            this.GroupBox_Grid.Controls.Add(this.Button_Column_Inc);
            this.GroupBox_Grid.Controls.Add(this.TextBox_Row);
            this.GroupBox_Grid.Controls.Add(this.Label_Color);
            this.GroupBox_Grid.Controls.Add(this.Label_Row);
            this.GroupBox_Grid.Controls.Add(this.TextBox_Column);
            this.GroupBox_Grid.Controls.Add(this.Label_Column);
            this.GroupBox_Grid.Location = new System.Drawing.Point(4, 12);
            this.GroupBox_Grid.Name = "GroupBox_Grid";
            this.GroupBox_Grid.Size = new System.Drawing.Size(112, 93);
            this.GroupBox_Grid.TabIndex = 0;
            this.GroupBox_Grid.TabStop = false;
            this.GroupBox_Grid.Text = "그리드";
            // 
            // PictureBox_Color
            // 
            this.PictureBox_Color.BackColor = System.Drawing.Color.Black;
            this.PictureBox_Color.Location = new System.Drawing.Point(30, 65);
            this.PictureBox_Color.Name = "PictureBox_Color";
            this.PictureBox_Color.Size = new System.Drawing.Size(30, 21);
            this.PictureBox_Color.TabIndex = 6;
            this.PictureBox_Color.TabStop = false;
            // 
            // Button_Row_Dec
            // 
            this.Button_Row_Dec.Location = new System.Drawing.Point(29, 41);
            this.Button_Row_Dec.Name = "Button_Row_Dec";
            this.Button_Row_Dec.Size = new System.Drawing.Size(20, 20);
            this.Button_Row_Dec.TabIndex = 5;
            this.Button_Row_Dec.Text = "-";
            this.Button_Row_Dec.UseVisualStyleBackColor = true;
            // 
            // Button_Column_Dec
            // 
            this.Button_Column_Dec.Location = new System.Drawing.Point(29, 14);
            this.Button_Column_Dec.Name = "Button_Column_Dec";
            this.Button_Column_Dec.Size = new System.Drawing.Size(20, 20);
            this.Button_Column_Dec.TabIndex = 5;
            this.Button_Column_Dec.Text = "-";
            this.Button_Column_Dec.UseVisualStyleBackColor = true;
            // 
            // Button_Row_Inc
            // 
            this.Button_Row_Inc.Location = new System.Drawing.Point(85, 41);
            this.Button_Row_Inc.Name = "Button_Row_Inc";
            this.Button_Row_Inc.Size = new System.Drawing.Size(20, 20);
            this.Button_Row_Inc.TabIndex = 4;
            this.Button_Row_Inc.Text = "+";
            this.Button_Row_Inc.UseVisualStyleBackColor = true;
            // 
            // Button_Column_Inc
            // 
            this.Button_Column_Inc.Location = new System.Drawing.Point(85, 14);
            this.Button_Column_Inc.Name = "Button_Column_Inc";
            this.Button_Column_Inc.Size = new System.Drawing.Size(20, 20);
            this.Button_Column_Inc.TabIndex = 4;
            this.Button_Column_Inc.Text = "+";
            this.Button_Column_Inc.UseVisualStyleBackColor = true;
            // 
            // TextBox_Row
            // 
            this.TextBox_Row.Location = new System.Drawing.Point(52, 40);
            this.TextBox_Row.Name = "TextBox_Row";
            this.TextBox_Row.Size = new System.Drawing.Size(30, 21);
            this.TextBox_Row.TabIndex = 2;
            this.TextBox_Row.Text = "1";
            this.TextBox_Row.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label_Color
            // 
            this.Label_Color.AutoSize = true;
            this.Label_Color.Location = new System.Drawing.Point(7, 70);
            this.Label_Color.Name = "Label_Color";
            this.Label_Color.Size = new System.Drawing.Size(17, 12);
            this.Label_Color.TabIndex = 0;
            this.Label_Color.Text = "색";
            // 
            // Label_Row
            // 
            this.Label_Row.AutoSize = true;
            this.Label_Row.Location = new System.Drawing.Point(7, 45);
            this.Label_Row.Name = "Label_Row";
            this.Label_Row.Size = new System.Drawing.Size(17, 12);
            this.Label_Row.TabIndex = 0;
            this.Label_Row.Text = "행";
            // 
            // TextBox_Column
            // 
            this.TextBox_Column.Location = new System.Drawing.Point(52, 13);
            this.TextBox_Column.MaxLength = 2;
            this.TextBox_Column.Name = "TextBox_Column";
            this.TextBox_Column.Size = new System.Drawing.Size(30, 21);
            this.TextBox_Column.TabIndex = 2;
            this.TextBox_Column.Text = "1";
            this.TextBox_Column.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label_Column
            // 
            this.Label_Column.AutoSize = true;
            this.Label_Column.Location = new System.Drawing.Point(7, 18);
            this.Label_Column.Name = "Label_Column";
            this.Label_Column.Size = new System.Drawing.Size(17, 12);
            this.Label_Column.TabIndex = 0;
            this.Label_Column.Text = "열";
            // 
            // GroupBox_Scale
            // 
            this.GroupBox_Scale.Controls.Add(this.Button_Origin);
            this.GroupBox_Scale.Controls.Add(this.Button_Fit);
            this.GroupBox_Scale.Controls.Add(this.Button_ZoomOut);
            this.GroupBox_Scale.Controls.Add(this.Button_ZoomIn);
            this.GroupBox_Scale.Controls.Add(this.TextBox_Scale);
            this.GroupBox_Scale.Location = new System.Drawing.Point(4, 111);
            this.GroupBox_Scale.Name = "GroupBox_Scale";
            this.GroupBox_Scale.Size = new System.Drawing.Size(112, 104);
            this.GroupBox_Scale.TabIndex = 1;
            this.GroupBox_Scale.TabStop = false;
            this.GroupBox_Scale.Text = "확대/축소";
            // 
            // Button_Fit
            // 
            this.Button_Fit.Location = new System.Drawing.Point(9, 71);
            this.Button_Fit.Name = "Button_Fit";
            this.Button_Fit.Size = new System.Drawing.Size(96, 23);
            this.Button_Fit.TabIndex = 0;
            this.Button_Fit.Text = "화면에 맞춤";
            this.Button_Fit.UseVisualStyleBackColor = true;
            // 
            // Button_Origin
            // 
            this.Button_Origin.Location = new System.Drawing.Point(9, 47);
            this.Button_Origin.Name = "Button_Origin";
            this.Button_Origin.Size = new System.Drawing.Size(96, 23);
            this.Button_Origin.TabIndex = 1;
            this.Button_Origin.Text = "원본 크기";
            this.Button_Origin.UseVisualStyleBackColor = true;
            // 
            // Button_ZoomOut
            // 
            this.Button_ZoomOut.Location = new System.Drawing.Point(8, 20);
            this.Button_ZoomOut.Name = "Button_ZoomOut";
            this.Button_ZoomOut.Size = new System.Drawing.Size(20, 20);
            this.Button_ZoomOut.TabIndex = 5;
            this.Button_ZoomOut.Text = "-";
            this.Button_ZoomOut.UseVisualStyleBackColor = true;
            // 
            // Button_ZoomIn
            // 
            this.Button_ZoomIn.Location = new System.Drawing.Point(84, 20);
            this.Button_ZoomIn.Name = "Button_ZoomIn";
            this.Button_ZoomIn.Size = new System.Drawing.Size(20, 20);
            this.Button_ZoomIn.TabIndex = 4;
            this.Button_ZoomIn.Text = "+";
            this.Button_ZoomIn.UseVisualStyleBackColor = true;
            // 
            // TextBox_Scale
            // 
            this.TextBox_Scale.Location = new System.Drawing.Point(29, 20);
            this.TextBox_Scale.MaxLength = 3;
            this.TextBox_Scale.Name = "TextBox_Scale";
            this.TextBox_Scale.Size = new System.Drawing.Size(54, 21);
            this.TextBox_Scale.TabIndex = 2;
            this.TextBox_Scale.Text = "100";
            this.TextBox_Scale.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ToolBoxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(120, 261);
            this.Controls.Add(this.GroupBox_Scale);
            this.Controls.Add(this.GroupBox_Grid);
            this.Name = "ToolBoxForm";
            this.ShowIcon = false;
            this.Text = "도구상자";
            this.GroupBox_Grid.ResumeLayout(false);
            this.GroupBox_Grid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Color)).EndInit();
            this.GroupBox_Scale.ResumeLayout(false);
            this.GroupBox_Scale.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBox_Grid;
        private System.Windows.Forms.Button Button_Row_Dec;
        private System.Windows.Forms.Button Button_Column_Dec;
        private System.Windows.Forms.Button Button_Row_Inc;
        private System.Windows.Forms.Button Button_Column_Inc;
        private System.Windows.Forms.TextBox TextBox_Row;
        private System.Windows.Forms.Label Label_Row;
        private System.Windows.Forms.TextBox TextBox_Column;
        private System.Windows.Forms.Label Label_Column;
        private System.Windows.Forms.ColorDialog ColorDialogue;
        private System.Windows.Forms.Label Label_Color;
        private System.Windows.Forms.PictureBox PictureBox_Color;
        private System.Windows.Forms.GroupBox GroupBox_Scale;
        private System.Windows.Forms.Button Button_Origin;
        private System.Windows.Forms.Button Button_Fit;
        private System.Windows.Forms.Button Button_ZoomOut;
        private System.Windows.Forms.Button Button_ZoomIn;
        private System.Windows.Forms.TextBox TextBox_Scale;
    }
}