﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Grid_Viewer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            this.Resize += new EventHandler(MainForm_Resize); // 보류
            this.pictureBox.Resize += new EventHandler(PictureBox_Resize);
            this.gridBox.MouseEnter += new EventHandler(PictureBox_MouseEnter);
            this.gridBox.MouseLeave += new EventHandler(PictureBox_MouseLeave);
            this.gridBox.MouseDown += new MouseEventHandler(PictureBox_MouseDown);
            this.gridBox.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
            this.gridBox.MouseUp += new MouseEventHandler(PictureBox_MouseUp);
            this.gridBox.Paint += new PaintEventHandler(GridBox_Paint);
            this.menuItem_File.Click += new EventHandler(OpenFile);
            this.menuItem_Tool.Click += new EventHandler(MenuItem_Tool_Click);

            gridBox.Parent = pictureBox;
            gridBox.BackColor = Color.Transparent;
            gridBox.Location = new Point(0, 0);
            gridBox.Visible = false;

            OpenFileDialogue.Title = "파일 열기";
            OpenFileDialogue.CheckFileExists = true;
            OpenFileDialogue.CheckPathExists = true;
            OpenFileDialogue.Filter = "JPG files (*.jpg)|*.jpg|PNG files (*.png)|*.png|BMP files (*.bmp)|*.bmp";
            OpenFileDialogue.FilterIndex = 1;
            OpenFileDialogue.RestoreDirectory = true;

            _ImageData = new ImageData();
            _ImageData.UpdateGrid = new ImageData.VoidDelegate(DrawGrid);
            _ImageData.UpdateImage = new ImageData.VoidDelegate(UpdateImage);
            NormalizedLocation = new PointF(0.5F, 0.5F);
            Drag = false;
            ToolBoxClosed = true;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ShowToolBox();
        }

        private void ToolBox_Closed(Object sender, FormClosedEventArgs e)
        {
            ToolBoxClosed = true;
        } 

        private void MainForm_Resize(Object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Minimized)
            {
                if(!ToolBoxClosed && this.ToolBox.Visible != false)
                {
                    this.ToolBox.Visible = false;
                }
            }
            else
            {
                if (!ToolBoxClosed && this.ToolBox.Visible != true)
                {
                    this.ToolBox.Visible = true;
                }
                PictureBox_Reposition();
            }
        }

        private void PictureBox_Resize(Object sender, EventArgs e)
        {
            PictureBox_Reposition();
            gridBox.Size = pictureBox.Size;
        }

        public void PictureBox_Reposition()
        {
            pictureBox.Location = new Point((int)(this.Width * NormalizedLocation.X) - (pictureBox.Width / 2), (int)(this.Height * NormalizedLocation.Y) - (pictureBox.Height / 2));
        }

        private void PictureBox_MouseEnter(Object sender, EventArgs e)
        {
            switch(_ImageData.MouseMode)
            {
                case ImageData.EMouseMode.MOVE:
                    gridBox.Cursor = Cursors.SizeAll;
                    break;
                case ImageData.EMouseMode.SCALE:
                    break;
            }
        }

        private void PictureBox_MouseLeave(Object sender, EventArgs e)
        {
            gridBox.Cursor = Cursors.Default;
        }

        private void PictureBox_MouseDown(Object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Drag = true;
                BeginLocation = new Point(e.X, e.Y);
            }
        }

        private void PictureBox_MouseMove(Object sender, MouseEventArgs e)
        {
            if (!Drag) return;

            pictureBox.Location = new Point(pictureBox.Location.X + e.X - BeginLocation.X, pictureBox.Location.Y + e.Y - BeginLocation.Y);
        }

        private void PictureBox_MouseUp(Object sender, MouseEventArgs e)
        {
            if(Drag)
            {
                NormalizedLocation.X = (float)(pictureBox.Location.X + pictureBox.Width / 2) / this.Width;
                NormalizedLocation.Y = (float)(pictureBox.Location.Y + pictureBox.Height / 2) / this.Height;
            }
            Drag = false;
        }

        private void GridBox_Paint(Object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(_ImageData.GridColor);
            foreach(ImageData.Grid grid in _ImageData.ColumnList)
            {
                e.Graphics.DrawLine(pen, grid.pt1, grid.pt2);
            }
            foreach (ImageData.Grid grid in _ImageData.RowList)
            {
                e.Graphics.DrawLine(pen, grid.pt1, grid.pt2);
            }
            pen.Dispose();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            this.ToolBox.TopMost = false;

            if(OpenFileDialogue.ShowDialog() == DialogResult.OK)
            {
                _ImageData.ImageSrc = new Bitmap(OpenFileDialogue.FileName);
                gridBox.Visible = true;
            }

            this.ToolBox.TopMost = true;
        }

        private void DrawGrid()
        {
            gridBox.Invalidate();
        }

        private void UpdateImage()
        {
            pictureBox.Image = _ImageData.ImageShow;
        }

        private void ShowToolBox()
        {
            if (!ToolBoxClosed) return;
            this.ToolBox = new ToolBoxForm(_ImageData, this);
            this.ToolBox.GridUpdate += new ToolBoxForm.GridUpdater(DrawGrid);
            this.ToolBox.FormClosed += new FormClosedEventHandler(ToolBox_Closed);
            this.ToolBox.TopMost = true;
            this.ToolBox.StartPosition = FormStartPosition.Manual;
            this.ToolBox.Location = new Point(this.Location.X + 10, this.Location.Y + 70);
            this.ToolBox.Show();
            ToolBoxClosed = false;
        }

        private void MenuItem_Tool_Click(Object sender, EventArgs e)
        {
            ShowToolBox();
        }

        private ImageData _ImageData;
        private ToolBoxForm ToolBox;
        private bool ToolBoxClosed;
        public PointF NormalizedLocation;
        private bool Drag;
        private Point BeginLocation;
    }
}
