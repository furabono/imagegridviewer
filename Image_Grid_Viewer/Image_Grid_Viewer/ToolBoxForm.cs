﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Grid_Viewer
{
    public partial class ToolBoxForm : Form
    {
        public ToolBoxForm(ImageData _ImageData, MainForm _ParentForm)
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.ShowInTaskbar = false;

            this.TextBox_Column.Text = _ImageData.Column.ToString();
            this.TextBox_Row.Text = _ImageData.Row.ToString();
            this.TextBox_Column.KeyPress += new KeyPressEventHandler(TextBox_FilterDigit);
            this.TextBox_Column.TextChanged += new EventHandler(TextBox_Column_TextChanged);
            this.TextBox_Column.LostFocus += new EventHandler(TextBox_Column_LostFocus);
            this.TextBox_Row.KeyPress += new KeyPressEventHandler(TextBox_FilterDigit);
            this.TextBox_Row.TextChanged += new EventHandler(TextBox_Row_TextChanged);
            this.TextBox_Row.LostFocus += new EventHandler(TextBox_Row_LostFocus);
            this.Button_Column_Dec.Click += new EventHandler(Button_Column_Dec_Click);
            this.Button_Column_Inc.Click += new EventHandler(Button_Column_Inc_Click);
            this.Button_Row_Dec.Click += new EventHandler(Button_Row_Dec_Click);
            this.Button_Row_Inc.Click += new EventHandler(Button_Row_Inc_Click);

            this.PictureBox_Color.BackColor = _ImageData.GridColor;
            this.PictureBox_Color.Click += new EventHandler(PictureBox_Color_Click);

            this.TextBox_Scale.Text = _ImageData.ScaleFactor.ToString();
            this.TextBox_Scale.KeyPress += new KeyPressEventHandler(TextBox_FilterDigit);
            this.TextBox_Scale.TextChanged += new EventHandler(TextBox_Scale_TextChanged);
            this.TextBox_Scale.LostFocus += new EventHandler(TextBox_Scale_LostFocus);

            this.Button_ZoomOut.Click += new EventHandler(Button_ZoomOut_Click);
            this.Button_ZoomIn.Click += new EventHandler(Button_ZoomIn_Click);

            this.Button_Origin.Click += new EventHandler(Button_Origin_Click);
            this.Button_Fit.Click += new EventHandler(Button_Fit_Click);

            this._ParentForm = _ParentForm;
            this._ImageData = _ImageData;
        }

        private void TextBox_FilterDigit(Object sender, KeyPressEventArgs e)
        {
            if(!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
            {
                e.Handled = true;
            }
        }

        private void TextBox_Column_TextChanged(Object sender, EventArgs e)
        {
            int col;
            if (TextBox_Column.Text.Length == 0) col = 1;
            else col = Convert.ToInt32(TextBox_Column.Text);

            if (col == _ImageData.Column) return;

            if(col < 1 || col > 10)
            {
                TextBox_Column.Text = _ImageData.Column.ToString();
                MessageBox.Show("1~10 사이의 값을 입력하세요");
                return;
            }

            _ImageData.Column = col;
        }

        private void TextBox_Row_TextChanged(Object sender, EventArgs e)
        {
            int row;
            if (TextBox_Row.Text.Length == 0) row = 1;
            else row = Convert.ToInt32(TextBox_Row.Text);
            if (row == _ImageData.Row) return;

            if (row < 1 || row > 10)
            {
                TextBox_Row.Text = _ImageData.Row.ToString();
                MessageBox.Show("1~10 사이의 값을 입력하세요");
                return;
            }

            _ImageData.Row = row;
        }

        private void TextBox_Column_LostFocus(Object sender, EventArgs e)
        {
            if (TextBox_Column.Text.Length == 0) TextBox_Column.Text = "1";
        }

        private void TextBox_Row_LostFocus(Object sender, EventArgs e)
        {
            if (TextBox_Row.Text.Length == 0) TextBox_Row.Text = "1";
        }

        private void Button_Column_Dec_Click(Object sender, EventArgs e)
        {
            if (_ImageData.Column == 1) return;
            this.TextBox_Column.Text = (_ImageData.Column - 1).ToString();
        }

        private void Button_Column_Inc_Click(Object sender, EventArgs e)
        {
            if (_ImageData.Column == 10) return;
            this.TextBox_Column.Text = (_ImageData.Column + 1).ToString();
        }

        private void Button_Row_Dec_Click(Object sender, EventArgs e)
        {
            if (_ImageData.Row == 1) return;
            this.TextBox_Row.Text = (_ImageData.Row - 1).ToString();
        }

        private void Button_Row_Inc_Click(Object sender, EventArgs e)
        {
            if (_ImageData.Row == 10) return;
            this.TextBox_Row.Text = (_ImageData.Row + 1).ToString();
        }

        private void PictureBox_Color_Click(Object sender, EventArgs e)
        {
            if(ColorDialogue.ShowDialog() == DialogResult.OK)
            {
                PictureBox_Color.BackColor = ColorDialogue.Color;
                _ImageData.GridColor = ColorDialogue.Color;
            }
        }

        private void TextBox_Scale_TextChanged(Object sender, EventArgs e)
        {
            if (TextBox_Scale.Text.Length == 0) return;

            int scale = Convert.ToInt32(TextBox_Scale.Text);
            if (scale == 0) return;
            _ImageData.ScaleFactor = Convert.ToInt32(TextBox_Scale.Text);
        }

        private void TextBox_Scale_LostFocus(Object sender, EventArgs e)
        {
            if (TextBox_Scale.Text.Length == 0 || Convert.ToInt32(TextBox_Scale.Text) == 0) TextBox_Scale.Text = _ImageData.ScaleFactor.ToString();
        }

        private void Button_ZoomIn_Click(Object sender, EventArgs e)
        {
            TextBox_Scale.Text = (_ImageData.ScaleFactor + 25).ToString();
        }

        private void Button_ZoomOut_Click(Object sender, EventArgs e)
        {
            if (_ImageData.ScaleFactor <= 25) return;
            TextBox_Scale.Text = (_ImageData.ScaleFactor - 25).ToString();
        }

        private void Button_Origin_Click(Object sender, EventArgs e)
        {
            TextBox_Scale.Text = (100).ToString();
        }

        private void Button_Fit_Click(Object sender, EventArgs e)
        {
            if (!_ImageData.ImageLoaded) return;
            float img_ratio = (float)_ImageData.ImageSrc.Width / _ImageData.ImageSrc.Height;
            float scr_ratio = (float)_ParentForm.ClientSize.Width / _ParentForm.ClientSize.Height;
            if(img_ratio > scr_ratio)
            {
                TextBox_Scale.Text = (100 * _ParentForm.ClientSize.Width / _ImageData.ImageSrc.Width).ToString();
            }
            else
            {
                TextBox_Scale.Text = (100 * _ParentForm.ClientSize.Height / _ImageData.ImageSrc.Height).ToString();
            }
            _ParentForm.NormalizedLocation = new PointF(0.5F, 0.5F);
            _ParentForm.PictureBox_Reposition();
        }

        MainForm _ParentForm;
        ImageData _ImageData;
        public delegate void GridUpdater();
        public GridUpdater GridUpdate;
    }
}
