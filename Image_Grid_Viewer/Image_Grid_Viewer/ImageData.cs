﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections;

namespace Image_Grid_Viewer
{
    public class ImageData
    {
        public ImageData()
        {
            _ImageLoaded = false;
            _ColumnList = new ArrayList();
            _RowList = new ArrayList();
            _Column = 1;
            _Row = 1;
            _GridColor = Color.Black;
            MouseMode = EMouseMode.MOVE;
            ScaleFactor = 100;
        }

        public delegate void VoidDelegate();

        private bool _ImageLoaded;
        public bool ImageLoaded
        {
            get
            {
                return _ImageLoaded;
            }
        }

        public VoidDelegate UpdateImage;

        private Bitmap _ImageSrc;
        public Bitmap ImageSrc
        {
            get
            {
                return _ImageSrc;
            }
            set
            {
                if(_ImageLoaded)
                {
                    _ImageSrc.Dispose();
                    _ImageShow.Dispose();
                }
                _ImageSrc = value;
                _ImageShow = new Bitmap(_ImageSrc);
                _ImageLoaded = true;
                UpdateImage();
                RedrawColumn();
                RedrawRow();
            }
        }

        private Bitmap _ImageShow;
        public Bitmap ImageShow
        {
            get
            {
                return _ImageShow;
            }
        }

        private int _Column;
        public int Column
        {
            get
            {
                return _Column;
            }
            set
            {
                if (value == _Column) return;
                _Column = value;

                RedrawColumn();
            }
        }
        private void RedrawColumn()
        {
            if (!_ImageLoaded) return;
            int height = _ImageShow.Height;
            float col_gap = (float)_ImageShow.Width / _Column;
            float x;
            _ColumnList.Clear();
            for (int i = 1; i < _Column; ++i)
            {
                x = col_gap * i;
                _ColumnList.Add(new Grid(x, 0, x, height));
            }
            UpdateGrid();
        }

        private int _Row;
        public int Row
        {
            get
            {
                return _Row;
            }
            set
            {
                if (value == _Row) return;
                _Row = value;

                RedrawRow();
            }
        }
        private void RedrawRow()
        {
            if (!_ImageLoaded) return;
            int width = _ImageShow.Width;
            float row_gap = (float)_ImageShow.Height / _Row;
            float y;
            _RowList.Clear();
            for (int i = 1; i < _Row; ++i)
            {
                y = row_gap * i;
                _RowList.Add(new Grid(0, y, width, y));
            }
            UpdateGrid();
        }

        public VoidDelegate UpdateGrid;

        private ArrayList _ColumnList;
        public ArrayList ColumnList
        {
            get
            {
                return _ColumnList;
            }
        }
        private ArrayList _RowList;
        public ArrayList RowList
        {
            get
            {
                return _RowList;
            }
        }

        public class Grid
        {
            public PointF pt1;
            public PointF pt2;
            public Grid(float x1, float y1, float x2,  float y2)
            {
                pt1 = new PointF(x1, y1);
                pt2 = new PointF(x2, y2);
            }
        }

        private Color _GridColor;
        public Color GridColor
        {
            get
            {
                return _GridColor;
            }
            set
            {
                if (_GridColor == value) return;
                _GridColor = value;
                UpdateGrid();
            }
        }

        public enum EMouseMode
        {
            MOVE, SCALE
        };

        public EMouseMode MouseMode;

        private int _ScaleFactor;
        public int ScaleFactor
        {
            get
            {
                return _ScaleFactor;
            }
            set
            {
                if (_ScaleFactor == value) return;
                _ScaleFactor = value;

                if (!_ImageLoaded) return;
                _ImageShow.Dispose();
                _ImageShow = new Bitmap(ImageSrc, new Size(ImageSrc.Width * ScaleFactor / 100, ImageSrc.Height * ScaleFactor / 100));
                UpdateImage();
                RedrawColumn();
                RedrawRow();
                UpdateGrid();
            }
        }
    }
}
